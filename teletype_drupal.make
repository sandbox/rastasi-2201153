; Core version
; ------------
core = 7.x
; API version
; ------------
api = 2
; Core project
; ------------
projects[] = drupal
projects[] = l10n_install
; Themes
; --------
projects[] = tao
projects[] = rubik
projects[] = bootstrap
; Essentials
; --------
projects[] = ctools
projects[] = token
projects[] = context
projects[] = panels
; Other stuffs
; --------
projects[] = admin_menu
projects[] = webform
projects[] = backup_migrate
; Features
; --------
projects[] = features
projects[] = strongarm
; Fields
projects[] = email
projects[] = link
projects[] = date
projects[] = references
; Views
; --------
projects[] = views
projects[] = views_slideshow
projects[] = views_fluid_grid
projects[] = calendar
; Editor
; --------
projects[] = print
projects[] = ckeditor
projects[] = imce

; Gallery
; --------
projects[] = galleryformatter
projects[] = plupload
projects[] = imce_filefield
projects[] = imce_plupload
projects[] = imce_mkdir

;Newsletter
; --------
projects[] = simplenews
projects[] = mimemail
; SEO
; --------
projects[] = xmlsitemap
projects[] = google_analytics
projects[] = page_title
; Libraries
; ---------
libraries[ckeditor][download][type] = "file"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.6.2/ckeditor_3.6.2.zip"

